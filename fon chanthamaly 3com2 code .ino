#include <Servo.h>

// Define the pin for the potentiometer
const int potPin = A0;

// Define the pin for the servo motor
const int servoPin = 9;

// Create a servo object
Servo myServo;

void setup() {
  // Attach the servo to the corresponding pin
  myServo.attach(servoPin);
}

void loop() {
  // Read the value of the potentiometer (0-1023)
  int potValue = analogRead(potPin);

  // Map the potentiometer value to the servo angle (0-180)
  int servoAngle = map(potValue, 0, 1023, 0, 180);

  // Set the servo angle based on the mapped value
  myServo.write(servoAngle);

  // Optional delay for smoother movement
  delay(15);
}